import React from 'react';
import {Button, ListGroupItem, Nav} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const QuoteItem = props => {
  return (
    <ListGroupItem header={props.header}>
      {props.children}
      <div>
        <Button onClick={props.delete}>remove</Button>
        <LinkContainer to={`/quote-edit/${props.id}`}>
          <Button>edit</Button>
        </LinkContainer>
      </div>
    </ListGroupItem>
  );
};

export default QuoteItem;