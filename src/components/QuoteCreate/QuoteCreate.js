import React from 'react';
import {Button, ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import axios from 'axios';
import loader from '../../preloader.gif';

class QuoteCreate extends React.Component {
  state = {
    category: this.props.category[0].replace(/\s/ig, '-').toLowerCase(),
    loader: false
  };

  sendForm = event => {
    event.preventDefault();
    this.state.loader = true;
    const data = {category: this.state.category, author: this.state.author, text: this.state.value};

    this.setState({loader: true});
    this.props.match.params.id ? axios.put(`/quote/${this.props.match.params.id}.json`, data)
      .then(() => {
        this.props.history.goBack();
      }): axios.post(`/quote.json`, data)
      .then(() => {
      this.props.history.push('/quote/all');
    });
  };

  handleInputChange = event => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  render() {
    return (
      <form onSubmit={this.sendForm}>
        <div hidden={!this.state.loader} style={
          {position: 'fixed',
            top: '0',
            left: '0',
            bottom: '0',
            right: '0',
            background: `#ffffff url(${loader}) no-repeat center`
          }}>
        </div>
        <FormGroup>
          <ControlLabel>Category</ControlLabel>
          <FormControl componentClass="select" placeholder="select"
                       name="category"
                       value={this.state.category}
                       onChange={this.handleInputChange}>
            {this.props.category.map(value => <option key={value.replace(/\s/ig, '-').toLowerCase()}
                                                      value={value.replace(/\s/ig, '-').toLowerCase()}>{value}</option>)}
          </FormControl>
          <ControlLabel>Author</ControlLabel>
          <FormControl
            type="text"
            name="author"
            value={this.state.author}
            onChange={this.handleInputChange}
            placeholder="Enter author"
          />
          <ControlLabel>Quote text</ControlLabel>
          <FormControl
            type="text"
            name="value"
            value={this.state.value}
            onChange={this.handleInputChange}
            placeholder="Enter quote"
          />
        </FormGroup>
        <Button disabled={!this.state.name && !this.state.value} type='submit'>Submit</Button>
      </form>
    );
  }
}

export default QuoteCreate;