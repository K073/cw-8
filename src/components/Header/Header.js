import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Header = props => {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
            <LinkContainer to='/'>
              <a>Quotes Central</a>
            </LinkContainer>
        </Navbar.Brand>
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav pullRight>
          <LinkContainer to='/quote/all'>
            <NavItem>
              Quotes
            </NavItem>
          </LinkContainer>
          <LinkContainer to='/quote-create'>
            <NavItem>
              Submit new quote
            </NavItem>
          </LinkContainer>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default Header