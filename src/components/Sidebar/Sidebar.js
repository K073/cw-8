import React from 'react';
import {Nav, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Sidebar = props => {
  return (
    <Nav>
      <LinkContainer to={'/quote/all'}><NavItem>All</NavItem></LinkContainer>
      {props.category.map(value => <LinkContainer key={value.replace(/\s/ig, '-').toLowerCase()} to={`/quote/${value.replace(/\s/ig, '-').toLowerCase()}`}><NavItem>{value}</NavItem></LinkContainer>)}
    </Nav>
  );
};

export default Sidebar;