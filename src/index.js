import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './container/App/App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from "react-router-dom";
import * as axios from "axios";

axios.defaults.baseURL = 'https://super-quote-list.firebaseio.com';

ReactDOM.render(<BrowserRouter ><App /></BrowserRouter>, document.getElementById('root'));
registerServiceWorker();