import React from 'react'
import {ListGroup, ListGroupItem} from "react-bootstrap";
import axios from 'axios';
import QuoteItem from "../../components/QuoteItem/QuoteItem";

class QuoteList extends React.Component{
  state = {
    list: []
  };

  getQuoteList = category => {
    const URL = category !== 'all' ? `?orderBy="category"&equalTo="${category}"` : '';
    this.setState({category: category})
    axios.get(`/quote.json${URL}`).then(res => {
      const data = res.data ? Object.keys(res.data).map(key => (
        {
          id: key,
          author: res.data[key].author,
          text: res.data[key].text,
          category: res.data[key].category
        }
      )) : [];
      this.setState(prevState => (
        prevState.list = data
      ));
      console.log(res.data);
    })
  };

  removeQuote = id => {
    axios.delete(`/quote/${id}.json`).then(() => {
      const list = [...this.state.list];
      list.splice(list.map(value => value.id).indexOf(id),1);
      this.setState({list});
    })
  };

  componentDidUpdate () {
    if (this.props.match.params.category !== this.state.category)
      this.getQuoteList(this.props.match.params.category)
  }
  componentDidMount () {
    this.getQuoteList(this.props.match.params.category)
  }
  render(){
    return (
      <ListGroup>
        {this.state.list.map(value => <QuoteItem key={value.id} id={value.id} delete={() => this.removeQuote(value.id)} header={value.author}>{value.text}</QuoteItem>)}
      </ListGroup>
    );
  }
};

export default QuoteList;