import React, { Component } from 'react';
import { Route} from "react-router-dom";
import Header from "../../components/Header/Header";
import Sidebar from "../../components/Sidebar/Sidebar";
import {Col, Grid, Row} from "react-bootstrap";
import QuoteList from "../QuoteList/QuoteList";
import QuoteCreate from "../../components/QuoteCreate/QuoteCreate";

class App extends Component {
  state = {
    category: ['Star Wars','Famous people','Saying','Humour','Motivational']
  };
  render() {
    return (
      <div>
        <Header/>
        <div className="container">
          <Grid>
            <Row>
              <Col md={2}>
                <Route path={'/quote'} render={props => (
                  <Sidebar category={this.state.category} {...props}/>
                )} />
              </Col>
              <Col md={10}>
                <Route path={'/quote/:category'} exact component={QuoteList}/>
              </Col>
            </Row>
          </Grid>
          <Route path={'/quote-create'} render={props => (
            <QuoteCreate category={this.state.category} {...props}/>
          )}/>
          <Route path={'/quote-edit/:id'} render={props => (
            <QuoteCreate category={this.state.category} {...props}/>
          )}/>
        </div>
      </div>
    );
  }
}

export default App;
